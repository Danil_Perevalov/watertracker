package com.princeparadoxes.watertracker.base;

public interface HasFragmentContainer {
    int fragmentsContainerId();
}
